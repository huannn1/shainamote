import Vue from 'vue'
import Router from 'vue-router'
import Shop from './views/Shop.vue'

Vue.use(Router)

export default new Router({
  mode: 'history',
  routes: [
    {
      path: '/',
      name: 'shop',
      component: Shop
    },
    {
      path: '/collections/:name?',
      name: 'collections',
      component: Shop
    },
    {
      path: '/blogs/:category?',
      name: 'blogs',
      component: () => import('./views/Blog.vue')
    },
    {
      path: '/about',
      name: 'about',
      component: () => import('./views/About.vue')
    },
    {
      path: '*',
      name: 'notFound',
      component: () => import('./views/NotFound.vue')
    }
  ]
})
