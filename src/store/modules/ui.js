// TODO: import api
import {
  TOGGLE_SEARCH,
  TOGGLE_DRAWER_LEFT,
  TOGGLE_DRAWER_RIGHT
} from '../mutation-types';

const state = {
  drawerLeftOpen: false,
  drawerRightOpen: false,
  searchOpen: false,
};

const getters = {
  drawerOpen: state => {
    return state.drawerLeftOpen || state.drawerRightOpen;
  },
  drawerLeftOpen: state => {
    return state.drawerLeftOpen;
  },
  drawerRightOpen: state => {
    return state.drawerRightOpen;
  },
  searchOpen: state => {
    return state.searchOpen;
  }
};

const actions = {
  toggleSearch({
    commit
  }) {
    commit(TOGGLE_SEARCH);
  },
  toggleDrawerLeft({
    commit
  }) {
    commit(TOGGLE_DRAWER_LEFT);
  },
  toggleDrawerRight({
    commit
  }) {
    commit(TOGGLE_DRAWER_RIGHT);
  }
};

const mutations = {
  [TOGGLE_SEARCH](state) {
    state.searchOpen = !state.searchOpen;
    state.drawerLeftOpen = false;
    state.drawerRightOpen = false;
  },
  [TOGGLE_DRAWER_LEFT](state) {
    state.drawerLeftOpen = !state.drawerLeftOpen;
    state.searchOpen = false;
    state.drawerRightOpen = false;
  },
  [TOGGLE_DRAWER_RIGHT](state) {
    state.drawerRightOpen = !state.drawerRightOpen;
    state.searchOpen = false;
    state.drawerLeftOpen = false;
  }
};

export default {
  namespaced: true,
  state,
  getters,
  actions,
  mutations,
}
